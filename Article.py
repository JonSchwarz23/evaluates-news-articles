from datetime import date

class Article:

    publication = ""
    publish_date = ""
    author = ""
    article = ""

    def __init__(self):
        pass

    def getArticle(self):
        return {"Publication" : self.publication, "Date" : self.publish_date.isoformat(),
                "Author" : self.author, "Article" : self.article[0:80] + "..."}
