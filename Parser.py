import re
from Article import Article
from datetime import date
import pickle

Months = ["January", "February", "March", "April", "May", "June", "July",
          "August", "September", "October", "Novermber", "December"]

def New_York_Times(file_name):
    with open('NYT.p', 'rb') as handle:
        Articles = pickle.load(handle)

    new_article = Article()
    new_article.publication = "The New York Times"
    adding = False
    file = open(file_name, 'r')

    while(True):

        line = file.readline().lstrip()
        split_line = line.split()

        if(len(split_line) > 0 and split_line[0] in Months and
        new_article.publish_date == ""):
            month = Months.index(split_line[0]) + 1
            day = int(split_line[1][:-1])
            year = int(split_line[2])
            new_article.publish_date = date(year, month, day)

        if len(split_line) > 0 and split_line[0] == "BYLINE:" and\
        split_line[1] == "By" and new_article.author == "":
            for x in split_line[2:]:
                new_article.author += x + " "

        if(re.match('LOAD-DATE: .*', line) != None):
            adding = False
            Articles += [new_article]
            new_article = Article()
            new_article.publication = "The New York Times"

        if adding == True and line != "":
            new_article.article += line[:-1] + " "

        if re.match('LENGTH: [0-9]+ words', line) != None:
            adding = True

        if re.match('^---- End of Request ----', line) != None:
            break

    file.close()

    with open('NYT.p', 'wb') as handle:
        pickle.dump(Articles, handle, protocol = pickle.HIGHEST_PROTOCOL)
    return Articles
