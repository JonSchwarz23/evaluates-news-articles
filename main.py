import Parser
import pprint
import pickle

def main():
    clear_data()
    Parser.New_York_Times("NYT2017.txt")
    print_data("NYT")

def clear_data():
    a = []
    with open('NYT.p', 'wb') as handle:
        pickle.dump(a, handle, protocol=pickle.HIGHEST_PROTOCOL)

def print_data(newspaper):
    pp = pprint.PrettyPrinter(indent=4)
    with open(newspaper + ".p", 'rb') as handle:
        Articles = pickle.load(handle)
        pp.pprint(list(map(lambda x: x.getArticle(), Articles)))





if __name__ == "__main__":
    main()
